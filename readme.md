## About

Laravel and Vue.js App for Adding and Viewing Posts

Copy the .env.example file and rename it to .env

Create the database and populate the .env file with appropriate database details.

run `composer update`

run `php artisan key:generate`

run `php artisan migrate`

run `php artisan db:seed` to populate the database with dummy data

Notice: If run on localhost, create and run on Virtual Host, in order to avoid any issues.