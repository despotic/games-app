<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'user'], function()
{
    // Create new Article
    Route::post('article', 'ArticleController@store');

    // Update Article
    Route::put('article', 'ArticleController@store');

    // Delete Article
    Route::delete('article/{id}', 'ArticleController@destroy');

    // Create new Category
    Route::post('category', 'CategoryController@store');

    // Update Category
    Route::put('category', 'CategoryController@store');

    // Create new Comment
    Route::post('comment', 'CommentController@store');

    // Update Comment
    Route::put('comment', 'CommentController@store');

    // Delete Comment
    Route::delete('comment/{id}', 'CommentController@destroy');

    // Update User
    Route::put('user', 'UserController@store');

    // Logout User
    Route::post('logout', 'UserController@logout');
});


// List Articles
Route::get('articles', 'ArticleController@index');

// List Single Article
Route::get('article/{id}', 'ArticleController@show');



//// List Categories
Route::get('categories', 'CategoryController@index');



//// List Comments
Route::get('comments', 'CommentController@index');



//// List Users
Route::get('users', 'UserController@index');

//// List Single User
Route::get('user/{id}', 'UserController@show');

// Create new User
Route::post('user', 'UserController@store');

// Login User
Route::post('login', 'UserController@login');