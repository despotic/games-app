<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(30),
        'body' => $faker->text(200),
        'category_id' => $faker->numberBetween(1, 10),
        'comments_count' => $faker->numberBetween(1, 10),
        'user_id' => $faker->numberBetween(1, 5),
        'cover_image' => 'images/sin1.jpg',
    ];
});
