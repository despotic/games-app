<!--header-->
<div class="header" >
    <div class="top-header" >
        <div class="container">
            <div class="top-head" >
                <ul class="header-in">
                    <li ><a href="#" >  Help</a></li>
                    <li><a href="contact.html">   Contact Us</a></li>
                    <li ><a href="#" >   How To Use</a></li>
                </ul>

                <div class="search">
                    <form>
                        <input type="text" value="search about something ?" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'search about something ?';}" >
                        <input type="submit" value="" >
                    </form>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!---->

    <div class="header-top">
        <div class="container">
            <div class="head-top">
                <div class="logo">
                    <h1><a href="index.html"><span> G</span>ames <span>C</span>enter</a></h1>

                </div>
                <div class="top-nav">
                    <span class="menu"><img src="images/menu.png" alt=""> </span>

                    <ul>
                        <li class="active"><a class="color1" href="index.html"  >Home</a></li>
                        <li><a class="color2" href="games.html"  >Games</a></li>
                        <li><a class="color3" href="reviews.html"  >Reviews</a></li>
                        <li><a class="color4" href="404.html" >News</a></li>
                        <li><a class="color5" href="blog.html"  >Blog</a></li>
                        <li><a class="color6" href="contact.html" >Contact</a></li>
                        <div class="clearfix"> </div>
                    </ul>

                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>