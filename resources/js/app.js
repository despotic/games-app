require('./bootstrap');

import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false;
Vue.use(require('vue-moment'));

new Vue({
    render: h => h(App),
    router,
    data: {
        user: {
            id: '',
            name: '',
            email: '',
            logged_in: ''
        },
        user_id: ''
    },
    created() {
        this.user_id = localStorage.getItem('user_id');
        if(this.user_id != undefined && this.user_id != '' && this.user_id != null) {
            this.fetchUser('/api/user/' + this.user_id);
        }
    },
    methods: {
        fetchUser(page_url) {
            fetch(page_url)
                .then(res => res.json())
                .then(res => {
                    this.user = res.data;
                })
                .catch(err => console.log(err));
        }
    }
}).$mount('#app')
