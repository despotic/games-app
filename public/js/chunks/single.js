(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/chunks/single"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Single.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Single.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      comments: [],
      comment: {
        id: '',
        subject: '',
        message: '',
        article_id: '',
        user_id: ''
      },
      article: {
        id: '',
        title: '',
        body: '',
        category_id: '',
        comments_count: '',
        user_id: '',
        created_at: ''
      },
      edit: false,
      root_data: this.$root.$data
    };
  },
  created: function created() {
    this.fetchComments('/api/comments');
    this.fetchArticle('/api/article/' + this.$route.params.id);
  },
  methods: {
    fetchArticle: function fetchArticle(page_url) {
      var _this = this;

      fetch(page_url).then(function (res) {
        return res.json();
      }).then(function (res) {
        _this.article = res.data;
      }).catch(function (err) {
        return console.log(err);
      });
    },
    fetchComments: function fetchComments(page_url) {
      var _this2 = this;

      fetch(page_url).then(function (res) {
        return res.json();
      }).then(function (res) {
        _this2.comments = res.data;
      }).catch(function (err) {
        return console.log(err);
      });
    },
    deleteComment: function deleteComment(id) {
      var _this3 = this;

      if (confirm('Are you sure?')) {
        fetch("/api/comment/".concat(id), {
          method: 'delete'
        }).then(function (res) {
          return res.json();
        }).then(function (data) {
          alert('Comment Removed');

          _this3.fetchComments('/api/comments');

          _this3.fetchArticle('/api/article/' + _this3.$route.params.id);
        }).catch(function (err) {
          return console.log(err);
        });
      }
    },
    addComment: function addComment() {
      var _this4 = this;

      if (this.edit === false) {
        // Add
        this.comment.user_id = this.$root.$data.user_id;
        this.comment.article_id = this.article.id;
        fetch('/api/comment', {
          method: 'post',
          body: JSON.stringify(this.comment),
          headers: {
            'content-type': 'application/json'
          }
        }).then(function (res) {
          return res.json();
        }).then(function (data) {
          _this4.comment.subject = '';
          _this4.comment.message = '';
          alert('Comment Added.');

          _this4.fetchComments('/api/comments');

          _this4.fetchArticle('/api/article/' + _this4.$route.params.id);
        }).catch(function (err) {
          return console.log(err);
        });
      } else {
        // Update
        fetch('/api/comment', {
          method: 'put',
          body: JSON.stringify(this.comment),
          headers: {
            'content-type': 'application/json'
          }
        }).then(function (res) {
          return res.json();
        }).then(function (data) {
          _this4.comment.subject = '';
          _this4.comment.message = '';
          alert('Comment Updated');

          _this4.fetchComments('/api/comments');

          _this4.fetchArticle('/api/article/' + _this4.$route.params.id);
        }).catch(function (err) {
          return console.log(err);
        });
      }
    },
    editComment: function editComment(comment) {
      this.edit = true;
      this.comment.id = comment.id;
      this.comment.subject = comment.subject;
      this.comment.message = comment.message;
      this.comment.article_id = comment.article_id;
      this.comment.user_id = comment.user_id;
      this.scrollToForm();
    },
    scrollToForm: function scrollToForm() {
      var comment_form = this.$el.querySelector("#comment-form");
      comment_form.scrollIntoView();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Single.vue?vue&type=template&id=261c4bb2&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Single.vue?vue&type=template&id=261c4bb2& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "single" }, [
      _c("div", { staticClass: "blog-to" }, [
        _c("img", {
          staticClass: "img-responsive sin-on",
          attrs: { src: "/" + _vm.article.cover_image, alt: "" }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "blog-top" }, [
          _c("div", { staticClass: "blog-left" }, [
            _c("b", [
              _vm._v(_vm._s(_vm._f("moment")(_vm.article.created_at, "D")))
            ]),
            _vm._v(" "),
            _c("span", [
              _vm._v(_vm._s(_vm._f("moment")(_vm.article.created_at, "MMMM")))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "top-blog" }, [
            _c("a", { staticClass: "fast", attrs: { href: "#" } }, [
              _vm._v(_vm._s(_vm.article.title))
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v("Posted by "),
              _c("a", { attrs: { href: "#" } }, [
                _vm._v(_vm._s(_vm.article.user_name))
              ]),
              _vm._v(" in "),
              _c("a", { attrs: { href: "#" } }, [
                _vm._v(_vm._s(_vm.article.category_name))
              ]),
              _vm._v(" | "),
              _c("a", { attrs: { href: "#" } }, [
                _vm._v(_vm._s(_vm.article.comments_count) + " Comments")
              ])
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "sed" }, [_vm._v(_vm._s(_vm.article.body))]),
            _vm._v(" "),
            _c("div", { staticClass: "clearfix" })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "clearfix" })
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "single-middle" },
        [
          _vm.article.comments_count
            ? _c("h3", [
                _vm._v(_vm._s(_vm.article.comments_count) + " Comments")
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm._l(_vm.comments, function(comment) {
            return comment.article_id === _vm.article.id
              ? _c("div", { key: comment.id, staticClass: "media" }, [
                  _c("div", { staticClass: "media-body" }, [
                    _vm.root_data.user.logged_in
                      ? _c("div", [
                          _c("h5", [
                            _c(
                              "a",
                              {
                                staticClass: "delete-comment",
                                attrs: { href: "#" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.deleteComment(comment.id)
                                  }
                                }
                              },
                              [_vm._v("Delete"), _c("span")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("h5", [
                            _c(
                              "a",
                              {
                                staticClass: "edit-comment",
                                attrs: { href: "#" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.editComment(comment)
                                  }
                                }
                              },
                              [_vm._v("Edit"), _c("span")]
                            )
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("h5", { staticClass: "media-heading mb" }, [
                      _c("a", { attrs: { href: "#" } }, [
                        _vm._v(_vm._s(comment.user_name))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("h4", { staticClass: "media-heading" }, [
                      _c("a", { attrs: { href: "#" } }, [
                        _vm._v(_vm._s(comment.subject))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("p", [_vm._v(_vm._s(comment.message))])
                  ])
                ])
              : _vm._e()
          })
        ],
        2
      ),
      _vm._v(" "),
      this.$root.$data.user.logged_in
        ? _c("div", { staticClass: "single-bottom" }, [
            _c("h3", [_vm._v("Leave A Comment")]),
            _vm._v(" "),
            _c(
              "form",
              {
                attrs: { id: "comment-form" },
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.addComment($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "comment" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.comment.subject,
                        expression: "comment.subject"
                      }
                    ],
                    attrs: { type: "text", placeholder: "Subject" },
                    domProps: { value: _vm.comment.subject },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.comment, "subject", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "clearfix" }),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.comment.message,
                      expression: "comment.message"
                    }
                  ],
                  attrs: { cols: "77", rows: "6", placeholder: "Message" },
                  domProps: { value: _vm.comment.message },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.comment, "message", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _c("input", { attrs: { type: "submit", value: "Send" } })
              ]
            )
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Single.vue":
/*!***************************************!*\
  !*** ./resources/js/views/Single.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Single_vue_vue_type_template_id_261c4bb2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Single.vue?vue&type=template&id=261c4bb2& */ "./resources/js/views/Single.vue?vue&type=template&id=261c4bb2&");
/* harmony import */ var _Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Single.vue?vue&type=script&lang=js& */ "./resources/js/views/Single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Single_vue_vue_type_template_id_261c4bb2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Single_vue_vue_type_template_id_261c4bb2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Single.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./resources/js/views/Single.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Single.vue?vue&type=template&id=261c4bb2&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/Single.vue?vue&type=template&id=261c4bb2& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_template_id_261c4bb2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Single.vue?vue&type=template&id=261c4bb2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Single.vue?vue&type=template&id=261c4bb2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_template_id_261c4bb2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Single_vue_vue_type_template_id_261c4bb2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);