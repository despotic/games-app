<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Get the category associated with the article.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the user associated with the article.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the comments associated with the article.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
