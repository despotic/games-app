<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Article;
use App\Http\Resources\Article as ArticleResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get articles
        $articles = Article::orderBy('created_at', 'desc')->paginate(5);

        // return collection of articles as a resource
        return ArticleResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = $request->isMethod('put') || $request->isMethod('patch')  ? Article::findOrFAil($request->article_id) : new Article;
        // Handle File Upload
        if($request->hasFile('cover_image')){
            $image = $request->file('cover_image');
            $name_with_extension = $image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $file_name = pathinfo($name_with_extension, PATHINFO_FILENAME);
            $tmp_path = $image->getPathName();

            $folder = 'images/';
            $full_file_name = $file_name.'_'.time().'.'.$extension;
            $new_path = public_path($folder).$full_file_name;

            File::move($tmp_path, $new_path);

            $fileNameToStore = 'images/'.$full_file_name;

        } else {
            $fileNameToStore = 'images/sin1.jpg';
        }

        $article->id = $request->input('article_id');
        $article->title = $request->input('title');
        $article->body = $request->input('body');
        $article->category_id = $request->input('category_id');
        $article->comments_count = $request->input('comments_count');
        $article->user_id = $request->input('user_id');
        if($request->hasFile('cover_image')) {
            $article->cover_image = $fileNameToStore;
        }

        if($article->save()) {
            return new ArticleResource($article);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get article
        $article = Article::findOrFail($id);

        // Return single article as a resource

        return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get article
        $article = Article::findOrFail($id);

        File::delete($article->cover_image);
        // Return deleted article as a resource
        if($article->delete()) {
            return new ArticleResource($article);
        }
    }
}
