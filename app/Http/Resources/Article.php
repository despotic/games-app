<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Article as ArticleModel;
use App\User as UserModel;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        if($request->isMethod('delete')) {
            $category_id = $this->category_id;
            $category_name = $this->category_name;
            $comments_count = 0;
        } else {
            $category = ArticleModel::find($this->id)->category;
            $category_id = $category->id;
            $category_name = $category->name;

            $comments_count = ArticleModel::find($this->id)->comments->where('article_id', $this->id)->count();
        }

        if($this->user_id) {
            $user = UserModel::find($this->user_id);
            $user_id = $user->id;
            $user_name = $user->name;
        } else {
            $user_id = $this->user_id;
            $user_name = $this->user_name;
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'category_name' => $category_name,
            'category_id' => $category_id,
            'comments_count' => $comments_count,
            'user_id' => $user_id,
            'user_name' => $user_name,
            'cover_image' => $this->cover_image,
            'created_at' => $this->created_at,
        ];

    }
}
