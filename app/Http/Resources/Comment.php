<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User as UserModel;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->user_id) {
            $user = UserModel::find($this->user_id);
            $user_id = $user->id;
            $user_name = $user->name;
        } else {
            $user_id = $this->user_id;
            $user_name = $this->user_name;
        }

        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'message' => $this->message,
            'article_id' => $this->article_id,
            'user_id' => $user_id,
            'user_name' => $user_name,
        ];

    }
}
