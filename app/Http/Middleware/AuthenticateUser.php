<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateUser
{
    public function handle($request, Closure $next)
    {
        if(session()->has('user_id')) {
            return $next($request);
        } else {
            return redirect('/');
        }
    }
}