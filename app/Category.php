<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Get the articles in a category.
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }
}
