<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * Get the articles of a user.
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    /**
     * Get the comments of a user.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
