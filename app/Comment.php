<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Get the article associated with the comment.
     */
    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    /**
     * Get the user associated with the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
